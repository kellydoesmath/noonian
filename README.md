Noonian -- a full-stack Javascript platform for web application development and data management.
==============================


Noonian is full-stack Javascript client/server platform for database management and general-purpose web-based development.


## Installation 

### Install Prerequisites

Before proceeding, ensure the following software packages are installed on the machine:

1. [Node.js](https://nodejs.org/en/download/)
2. [MongoDB](https://www.mongodb.com/download-center?jmp=nav#community)
3. [bower](https://bower.io)


### Pull from npm

```bash
npm install -g noonian
```


*NOTE: if you are installing to a system-wide node install (as opposed to a node installation that is owned by a non-root user), 
there may be issues installing dependencies compiled via node-gyp.  To get around the issue, use the "unsafe-perm" parameter:*

```bash
npm install --unsafe-perm -g noonian
```

*This will be resovled in a future version that will have refactored out any dependencies that are not pure javascript.*

## Instance set-up

[See wiki article](https://gitlab.com/ChiweenieDijon/noonian/wikis/Development-Guide/Getting-Started/Instance-Setup)

## License

GNU AFFERO GENERAL PUBLIC LICENSE, Version 3

Copyright (C) 2016-2019  Eugene Lockett  gene@noonian.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
